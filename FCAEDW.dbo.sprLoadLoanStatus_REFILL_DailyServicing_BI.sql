USE [FCAEDW]
GO

/****** Object:  StoredProcedure [dbo].[sprLoadLoanStatus_REFILL_DailyServicing_BI]    Script Date: 1/3/2022 11:33:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[sprLoadLoanStatus_REFILL_DailyServicing_BI] as

Declare @SnapshotDate date = DATEADD(day,DATEDIFF(day,0,GETDATE())-Day(GetDate()),0)
Declare @EndDate date =  Dateadd(day, -1, convert(date, getdate()))

Select Cust, Nbr as Nbr_Adj, MaxBank, filedt into #MaxNbr
from RAXSQl01.FCAEDW.dbo.tb_MaxNbr_CSC_HISTORY as #Mx
where filedt between @SnapshotDate and @EndDate


Truncate Table tb_CSC_LoanStatus_REFILL

Insert into tb_CSC_LoanStatus_REFILL

Select CSCRetailDW.[FileDt] as Filedatefmt
,CSCRetailDW.[Cust]
,CSCRetailDW.Nbr
,
(Case 
	when B.[Chargeoff Flag]= 1 then 'C' 
	when #MaxNbr.Nbr_adj <> CSCRetailDW.nbr then 'R' 
	--when CSCRetailDW.boardingamt is null then 'N' 
	when prinbal = '0000000.00' then 'P' 
else 'A' 
end) as Loan_Status
, MaxBank AS BANK
, '' AS BRANCH

from FCAEDW.dbo.CSCRetailDW

Inner join #MaxNbr 
on #MaxNbr.Cust = CSCRetailDW.cust and #MaxNbr.nbr_adj = CSCRetailDW.nbr and #MaxNbr.maxbank = CSCRetailDW.Bank
			AND #MaxNbr.FILEDT = CSCRetailDW.FileDt
		   
left outer join (select [CSC ID], [DateID], [Chargeoff Flag] 
				From FCAEDW.dbo.fcfchargeoffs) as B 
				on B.[CSC ID] = CSCRetailDW.cust and B.DateID =   
				(convert(varchar,year(dateadd(month,-1, dateadd(day, 1,CSCRetailDW.FileDt)))) 
				+ (case when month(dateadd(month,-1, dateadd(day, 1,CSCRetailDW.FileDt))) > 9 
				then convert(varchar, month(dateadd(month,-1, dateadd(day, 1,CSCRetailDW.FileDt)))) 
				else ('0' + convert(varchar, month(dateadd(month,-1, dateadd(day, 1,CSCRetailDW.FileDt))))) end ))


WHERE CSCRetailDW.FileDt Between DATEADD(day,DATEDIFF(day,0,GETDATE())-Day(GetDate()),0)  and DATEADD(day,DATEDIFF(day,0,GETDATE())-1,0)  --Use for days insert, remove for a full run

-------------------------------
--Debug
--------------
--order by Cust, nbr
--WHERE CSCRetailDW.cust = 0013334
--order by CSCRetailDW.cust, CSCRetailDW.filedt 



--The N values below will always be N, due to incorrect boarding amount.
update tb_CSC_LoanStatus_REFILL
set loan_status = 'N'
where cust in (5100060,
5100219,
5100292,
5100334,
5100409,
5100441,
5100706,
5100904,
5101027,
5101399,
5101811,
5102025,
5102199,
5102298,
5102496,
5103288,
5103494,
5103833,
5103916,
5104138,
5104575,
5104716,
5104849,
5104922,
5104963,
5105853,
5106042,
5106208,
5106224,
5106430,
5106505,
5107107
)

-----------------------------------
------DEBUG SECTION PULL THE ROWS TO BE UPDATED
      
----  Select A.*, B.* 
----  FROM tblCSCDWDetail_DailyServicing as A
----  Inner Join [FCABI].[dbo].[tb_CSC_LoanStatus_REFILL] as B
----  On A.CSC_id = b.Cust and A.nbr = B.Nbr and A.Filedt = B.Filedatefmt
----  --order by A.CSC_Id
----  Where  A.LoanStatus <> B.Loan_Status
----  order by A.CSC_ID
  
---------------------------------------------------------------
--UPDATE Daily Servicing with the new charge off table values.
---------------------------------------------------------------

  --Update Loan Status Full table using Loan Status Refill
  /*
  Update tb_CSC_LoanStatus_FULL
  Set Loan_Status = B.Loan_Status
  FROM tb_CSC_LoanStatus_FULL as A
  Inner Join [FCAEDW].[dbo].[tb_CSC_LoanStatus_REFILL] as B
  --Inner Join FCAEDW.portfolio.account_retail_master as B
  On A.cust = b.Cust and A.Nbr = B.Nbr and A.Filedatefmt = B.Filedatefmt
  Where  A.Loan_status <> B.Loan_Status
  */

  -- Reverted to updating loan status field in Acct Retail Master for monthly process - 04/01/2019 - SR
  Update portfolio.account_retail_master
  Set Loan_Status = B.Loan_Status
  FROM FCAEDW.portfolio.account_retail_master as A
  Inner Join [FCAEDW].[dbo].[tb_CSC_LoanStatus_REFILL] as B
  On A.CSC_id = b.Cust and A.nbr = B.Nbr and A.Filedt = B.Filedatefmt
  Where A.FileDt >= @SnapshotDate and -- added on 11/02/2020 - SR
  A.Loan_Status <> B.Loan_Status


  -- Update statement changed to a delete and reload, to improve speed - 02/02/2018 - SR

  Delete from [FCAEDW].[dbo].[tb_CSC_LoanStatus_Full]
  where Convert(date, Filedatefmt) >= convert(date, DATEADD(day,DATEDIFF(day,0,GETDATE())-Day(GetDate()),0)) 
 
  Insert into [FCAEDW].[dbo].[tb_CSC_LoanStatus_Full]
  
  Select * from [FCAEDW].[dbo].[tb_CSC_LoanStatus_REFILL]

   
-- Update Account Retail Master with new loan status values 
--Exec sp_LoadAccountRetailMaster_MonthlyRefresh



GO



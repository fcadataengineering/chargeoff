USE [FCABI]
GO

/****** Object:  StoredProcedure [dbo].[sprLoadLoanStatus_REFILL_DailyServicing_BI]    Script Date: 1/3/2022 11:30:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/****** Object:  StoredProcedure [dbo].[sprLoadLoanStatus_REFILL_DailyServicing_BI]    Script Date: 3/4/2019 9:57:16 PM ******/
CREATE procedure [dbo].[sprLoadLoanStatus_REFILL_DailyServicing_BI] as

Declare @SnapshotDate date = DATEADD(day,DATEDIFF(day,0,GETDATE())-Day(GetDate()),0)
Declare @EndDate date =  Dateadd(day, -1, convert(date, getdate()))

Select Cust, Nbr, Bank, Prinbal, filedt into #CSCRetailDW
from RAXSQL01.IMPORT_STORAGE.dxc.CSCRetailDW
where convert(date, filedt) between @SnapshotDate and @EndDate

Select Cust, Nbr as Nbr_Adj, MaxBank, filedt into #MaxNbr
from RAXSQl01.IMPORT_STORAGE.dbo.tb_MaxNbr_CSC as #Mx
where filedt between @SnapshotDate and @EndDate


Truncate Table tb_CSC_LoanStatus_REFILL

Insert into tb_CSC_LoanStatus_REFILL


select #CSCRetailDW.[FileDt] as Filedatefmt
, #CSCRetailDW.[Cust]
, #CSCRetailDW.Nbr
,
(Case 
	when B."Chargeoff Flag"= 1 then 'C' 
	when #MaxNbr.Nbr_adj <> #CSCRetailDW.Nbr then 'R' 
	--when CSCRetailDW.boardingamt is null then 'N' 
	when prinbal = '0000000.00' then 'P' 
else 'A' 
end) as Loan_Status
,MaxBank AS BANK
,'' AS BRANCH

from #CSCRetailDW 

Inner join #MaxNbr 
on #MaxNbr.Cust = #CSCRetailDW.cust and #MaxNbr.nbr_adj = #CSCRetailDW.nbr and #MaxNbr.maxbank = #CSCRetailDW.Bank
			AND #MaxNbr.FILEDT = #CSCRetailDW.FileDt

Left outer join (select "CSC ID", DateID, "Chargeoff Flag" 
				From FCABI.dbo.fcfchargeoffs) as B 
				on B."CSC ID" = #CSCRetailDW.cust and B.DateID =   
				(convert(varchar,year(dateadd("month",-1, dateadd("day", 1,#CSCRetailDW.FileDt)))) 
				+ (case when month(dateadd("month",-1, dateadd("day", 1,#CSCRetailDW.FileDt))) > 9 
				then convert(varchar, month(dateadd("month",-1, dateadd("day", 1, #CSCRetailDW.FileDt)))) 
				else ('0' + convert(varchar, month(dateadd("month",-1, dateadd("day", 1, #CSCRetailDW.FileDt))))) end ))

WHERE #CSCRetailDW.FileDt Between DATEADD(day,DATEDIFF(day,0,GETDATE())-Day(GetDate()),0)  and DATEADD(day,DATEDIFF(day,0,GETDATE())-1,0)  --Use for days insert, remove for a full run

-------------------------------
--Debug
--------------
--order by Cust, nbr
--WHERE CSCRetailDW.cust = 0013334
--order by CSCRetailDW.cust, CSCRetailDW.filedt 



--The N values below will always be N, due to incorrect boarding amount.
update tb_CSC_LoanStatus_REFILL
set loan_status = 'N'
where cust in (5100060,
5100219,
5100292,
5100334,
5100409,
5100441,
5100706,
5100904,
5101027,
5101399,
5101811,
5102025,
5102199,
5102298,
5102496,
5103288,
5103494,
5103833,
5103916,
5104138,
5104575,
5104716,
5104849,
5104922,
5104963,
5105853,
5106042,
5106208,
5106224,
5106430,
5106505,
5107107
)

-----------------------------------
------DEBUG SECTION PULL THE ROWS TO BE UPDATED
      
----  Select A.*, B.* 
----  FROM tblCSCDWDetail_DailyServicing as A
----  Inner Join [FCABI].[dbo].[tb_CSC_LoanStatus_REFILL] as B
----  On A.CSC_id = b.Cust and A.nbr = B.Nbr and A.Filedt = B.Filedatefmt
----  --order by A.CSC_Id
----  Where  A.LoanStatus <> B.Loan_Status
----  order by A.CSC_ID
  
---------------------------------------------------------------
--UPDATE Daily Servicing with the new charge off table values.
---------------------------------------------------------------

-- Commented below to reload the days, rather than Update - due to performance 
-- Uncommented below after adding the index on LoanStatus in DailyServicing table - 06/03/2019 - SR
--UPDATE tblCSCDWDetail_DailyServicing TABLE
  Update A
  Set LoanStatus = B.Loan_Status
  FROM tblCSCDWDetail_DailyServicing as A
  Inner Join [FCABI].[dbo].[tb_CSC_LoanStatus_REFILL] as B
  On A.CSC_id = b.Cust and A.nbr = B.Nbr and A.Filedt = B.Filedatefmt   
  Where  A.LoanStatus <> B.Loan_Status


-- Commented out below after adding the index on LoanStatus field in DailyServicing table - 06/03/2019 - SR
/*
Select * into #Temp_DailyServ 
from tblCSCDWDetail_DailyServicing
where Filedt in (Select distinct Filedatefmt from [FCABI].[dbo].[tb_CSC_LoanStatus_REFILL])

  Update #Temp_DailyServ
  Set LoanStatus = B.Loan_Status
  FROM #Temp_DailyServ as A
  Inner Join [FCABI].[dbo].[tb_CSC_LoanStatus_REFILL] as B
  On A.CSC_id = b.Cust and A.nbr = B.Nbr and A.Filedt = B.Filedatefmt
  Where  A.LoanStatus <> B.Loan_Status

  Delete from tblCSCDWDetail_DailyServicing
  where convert(date, Filedt) in (Select distinct Filedatefmt from [FCABI].[dbo].[tb_CSC_LoanStatus_REFILL])

  Insert into tblCSCDWDetail_DailyServicing
  Select * from #Temp_DailyServ
 */

 -- Commented below to reload the days, rather than Update - due to performance  
 /*  
  --UPDATE TABLE LOAN STATUS TABLE FULL JUST INCASE A REFILL OF THE Dailyservicing is needed to be refilled
  Update tb_CSC_LoanStatus_FULL
  Set loan_status = B.LoanStatus
  FROM tb_CSC_LoanStatus_FULL as A
  Inner Join tblCSCDWDetail_DailyServicing as B
  On A.cust = b.CSC_id and A.nbr = B.Nbr and A.Filedatefmt = B.Filedt
  Where  A.loan_status <> B.LoanStatus
*/

  Delete from tb_CSC_LoanStatus_FULL 
  where convert(date, Filedatefmt) in (Select distinct Filedatefmt from [FCABI].[dbo].[tb_CSC_LoanStatus_REFILL])

  Insert into tb_CSC_LoanStatus_FULL
  Select * from [FCABI].[dbo].[tb_CSC_LoanStatus_REFILL]



GO



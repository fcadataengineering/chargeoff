USE [IMPORT_STORAGE]
GO

/****** Object:  StoredProcedure [dbo].[sprARM_Monthly_LoanStatus_Update]    Script Date: 4/27/2022 3:36:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create proc [dbo].[sprARM_Monthly_LoanStatus_Update] as

Declare @SnapshotDate date = DATEADD(day,DATEDIFF(day,0,GETDATE())-Day(GetDate()),0)
Declare @EndDate date =  Dateadd(day, -1, convert(date, getdate()))

Select Cust, Nbr as Nbr_Adj, MaxBank, filedt into #MaxNbr
from IMPORT_STORAGE.dbo.tb_MaxNbr_CSC as #Mx
where filedt between @SnapshotDate and @EndDate

Truncate Table IMPORT_STORAGE.dbo.tb_CSC_LoanStatus_REFILL

Insert into IMPORT_STORAGE.dbo.tb_CSC_LoanStatus_REFILL

Select CSCRetailDW.[FileDt] as Filedatefmt
,CSCRetailDW.[Cust]
,CSCRetailDW.Nbr
,
(Case 
	when B.[Chargeoff Flag]= 1 then 'C' 
	when #MaxNbr.Nbr_adj <> CSCRetailDW.nbr then 'R' 
	--when CSCRetailDW.boardingamt is null then 'N' 
	when prinbal = '0000000.00' then 'P' 
else 'A' 
end) as Loan_Status
, MaxBank AS BANK
, '' AS BRANCH

from IMPORT_STORAGE.dxc.CSCRetailDW

Inner join #MaxNbr 
on #MaxNbr.Cust = CSCRetailDW.cust and #MaxNbr.nbr_adj = CSCRetailDW.nbr and #MaxNbr.maxbank = CSCRetailDW.Bank
			AND #MaxNbr.FILEDT = CSCRetailDW.FileDt
		   
left outer join (select [CSC ID], [DateID], [Chargeoff Flag] 
				From IMPORT_STAGING.dbo.fcfchargeoffs) as B 
				on B.[CSC ID] = CSCRetailDW.cust and B.DateID =   
				(convert(varchar,year(dateadd(month,-1, dateadd(day, 1,CSCRetailDW.FileDt)))) 
				+ (case when month(dateadd(month,-1, dateadd(day, 1,CSCRetailDW.FileDt))) > 9 
				then convert(varchar, month(dateadd(month,-1, dateadd(day, 1,CSCRetailDW.FileDt)))) 
				else ('0' + convert(varchar, month(dateadd(month,-1, dateadd(day, 1,CSCRetailDW.FileDt))))) end ))


WHERE CSCRetailDW.FileDt Between DATEADD(day,DATEDIFF(day,0,GETDATE())-Day(GetDate()),0)  and DATEADD(day,DATEDIFF(day,0,GETDATE())-1,0)  --Use for days insert, remove for a full run

-------------------------------
--Debug
--------------
--order by Cust, nbr
--WHERE CSCRetailDW.cust = 0013334
--order by CSCRetailDW.cust, CSCRetailDW.filedt 



--The N values below will always be N, due to incorrect boarding amount.
update IMPORT_STORAGE.dbo.tb_CSC_LoanStatus_REFILL
set loan_status = 'N'
where cust in (5100060,
5100219,
5100292,
5100334,
5100409,
5100441,
5100706,
5100904,
5101027,
5101399,
5101811,
5102025,
5102199,
5102298,
5102496,
5103288,
5103494,
5103833,
5103916,
5104138,
5104575,
5104716,
5104849,
5104922,
5104963,
5105853,
5106042,
5106208,
5106224,
5106430,
5106505,
5107107
)


--- Update FCAEDW table

Truncate table FCAEDW.dbo.tb_CSC_LoanStatus_REFILL

Insert into FCAEDW.dbo.tb_CSC_LoanStatus_REFILL
Select * from IMPORT_STORAGE.dbo.tb_CSC_LoanStatus_REFILL

  -- Reverted to updating loan status field in Acct Retail Master for monthly process - 04/01/2019 - SR
  Update A
  Set Loan_Status = B.Loan_Status
  FROM FCAEDW.portfolio.account_retail_master as A
  Inner Join IMPORT_STORAGE.[dbo].[tb_CSC_LoanStatus_REFILL] as B
  On A.CSC_id = b.Cust and A.nbr = B.Nbr and A.Filedt = B.Filedatefmt
  Where A.FileDt >= @SnapshotDate and A.Loan_Status <> B.Loan_Status

  -- Updating Import_Staging - ARM
  Update A
  Set Loan_Status = B.Loan_Status
  FROM Import_Staging.dbo.account_retail_master as A
  Inner Join IMPORT_STORAGE.[dbo].[tb_CSC_LoanStatus_REFILL] as B
  On A.CSC_id = b.Cust and A.nbr = B.Nbr and A.Filedt = B.Filedatefmt
  Where A.FileDt >= @SnapshotDate and 
  A.Loan_Status <> B.Loan_Status

  -- Updating Import_Storage - ARM
  Update A
  Set Loan_Status = B.Loan_Status
  FROM Import_Storage.dbo.account_retail_master as A
  Inner Join IMPORT_STORAGE.[dbo].[tb_CSC_LoanStatus_REFILL] as B
  On A.CSC_id = b.Cust and A.nbr = B.Nbr and A.Filedt = B.Filedatefmt
  Where A.FileDt >= @SnapshotDate and 
  A.Loan_Status <> B.Loan_Status

  -- Update statement changed to a delete and reload, to improve speed - 02/02/2018 - SR

  Delete from [FCAEDW].[dbo].[tb_CSC_LoanStatus_Full]
  where Convert(date, Filedatefmt) >= convert(date, DATEADD(day,DATEDIFF(day,0,GETDATE())-Day(GetDate()),0)) 
 
  Insert into [FCAEDW].[dbo].[tb_CSC_LoanStatus_Full]
  Select * from [FCAEDW].[dbo].[tb_CSC_LoanStatus_REFILL]

   -- Update LoanStatus in Import_Staging
  Truncate table IMPORT_STAGING.[dbo].[tb_CSC_LoanStatus]
  
  Insert into IMPORT_STAGING.[dbo].[tb_CSC_LoanStatus]
  Select * from IMPORT_STORAGE.[dbo].[tb_CSC_LoanStatus_REFILL]

  -- Update LoanStatus in Import_Storage
  Delete from IMPORT_STORAGE.[dbo].[tb_CSC_LoanStatus]
  where Convert(date, Filedatefmt) >= convert(date, DATEADD(day,DATEDIFF(day,0,GETDATE())-Day(GetDate()),0)) 
 
  Insert into IMPORT_STORAGE.[dbo].[tb_CSC_LoanStatus]
  Select * from IMPORT_STORAGE.[dbo].[tb_CSC_LoanStatus_REFILL]



GO


